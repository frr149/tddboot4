//
//  Money.swift
//  Money
//
//  Created by Fernando Rodríguez Romero on 4/17/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import Foundation

protocol Money {
    
    init(amount: Int, currency: Currency)

    
    func times(_ n:Int)->Self
    
    func plus(_ addend: Self)-> Self
    
    
    func reduced(to: Currency, broker: Rater) throws -> Bill
    
}
