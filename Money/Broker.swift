//
//  Broker.swift
//  Bill
//
//  Created by Fernando Rodríguez Romero on 4/17/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import Foundation

// Errors
enum BrokerErrors : Error{
    case unknownRate
}

// Typealiases
typealias RatesDictionary = [String:Rate]
typealias Rate = Int    // en el futuro, lo cambiaré a Double

// Protocols
protocol Rater{
    func rate(from: Currency, to: Currency) throws -> Rate
}

// Default implementations
extension Rater{
    func rate(from: Currency, to: Currency) throws -> Rate{
        return 1
    }
}

// Types
struct UnityBroker : Rater{}

struct Broker : Rater{
    
    var _rates = RatesDictionary()
    
    private func _makeKeyForRate(from: Currency, to: Currency)-> String{
        return "\(from)->(\to)"
    }
    
    
    mutating func addRate(from: Currency, to:Currency, rate: Rate)  {
        
        _rates[_makeKeyForRate(from: from, to: to)] = rate
        _rates[_makeKeyForRate(from: to, to: from)] = 1 / rate
        _rates[_makeKeyForRate(from: from, to: from)] = 1
        _rates[_makeKeyForRate(from: to, to: to)] = 1
        
    }
    
    func rate(from: Currency, to: Currency) throws -> Rate{
        guard let rate = _rates[_makeKeyForRate(from: from, to: to)] else{
            throw BrokerErrors.unknownRate
        }
        return rate
        
    }
    
}
